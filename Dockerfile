FROM node:12.16.1-stretch-slim

WORKDIR /opt

COPY ./sonar-scanner.zip /opt

RUN apt update && apt install unzip
RUN unzip sonar-scanner.zip
RUN rm sonar-scanner.zip
RUN mv sonar-scanner-* sonar-scanner
RUN ln -s /opt/sonar-scanner/bin/sonar-scanner /usr/local/bin/sonar-scanner

RUN sonar-scanner -v
RUN node -v
RUN yarn -v

WORKDIR /home/node

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["node"]
